package com.bewerbungWebsite.aidp.models;

import javax.persistence.*;
import java.util.Set;


@Entity
public class Applicant extends UserTopClass {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String firstName;
    @Column
    private String dayOfBirth;
    @Column
    private String gender;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<Application> application;

    @OneToOne(cascade = CascadeType.ALL)
    private Address address;

    public Applicant(String firstName, String dayOfBirth, String gender, String name, String email, String password, String phoneNumber) {
        super(name, email, password, phoneNumber);
        this.firstName = firstName;
        this.dayOfBirth = dayOfBirth;
        this.gender = gender;
    }

    public Applicant() {

    }

    public Applicant(String name, String email, String password) {
        super(name, email, password);
    }

    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getDayOfBirth() {
        return dayOfBirth;
    }

    public void setDayOfBirth(String dayOfBirth) {
        this.dayOfBirth = dayOfBirth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Set<Application> getApplication() {
        return application;
    }

    public void setApplication(Set<Application> application) {
        this.application = application;
    }
}
