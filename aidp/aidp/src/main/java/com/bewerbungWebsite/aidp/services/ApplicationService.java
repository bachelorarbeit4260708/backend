package com.bewerbungWebsite.aidp.services;


import com.bewerbungWebsite.aidp.dto.SchoolPositionDto;
import com.bewerbungWebsite.aidp.email.EmailSender;
import com.bewerbungWebsite.aidp.email.emailView.ApplicantEmail;
import com.bewerbungWebsite.aidp.email.emailView.SchoolEmail;
import com.bewerbungWebsite.aidp.models.*;
import com.bewerbungWebsite.aidp.repository.*;
import com.bewerbungWebsite.aidp.util.FileUtility;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;


@Service
@RequiredArgsConstructor
public class ApplicationService {

    @Autowired
    ApplicantRepo applicantRepo;

    @Autowired
    PositionRepo positionRepo;

    @Autowired
    ApplicationRepo applicationRepo;

    @Autowired
    ApplicationFileRepo applicationFileRepo;

    @Autowired
    NoAccountApplicationRepo noAccountApplicationRepo;


    private final EmailSender emailSender;


    private final ApplicantEmail applicantEmail;


    private final SchoolEmail schoolEmail;

    @Autowired
    SchoolRepo schoolRepo;


    public LocalDateTime generateDate() {
        LocalDateTime datum = LocalDateTime.now();
        return datum;
    }

    /* *
     * @param applicantId : Bewerber ID
     *  @param date : Bewerbungsdatum
     *  @param positionId : Ausbildungsplatz ID
     * mit applicantId wird herausgefunden welche Bewerber sich für eine Stelle (positionID) und wann genau(date) bewirbt
     */
    public Long createApplication(Long applicantId, LocalDateTime date, Long positionId) {

        Long id = 1L;
        if (applicantRepo.existsById(applicantId)) {
            if (positionRepo.existsById(positionId)) {
                Position position = positionRepo.findById(positionId).get();
                Application application = new Application(date);
                applicationRepo.save(application);
                application.setPosition(position);

                Applicant applicant = applicantRepo.findById(applicantId).get();
                applicant.getApplication().add(application);

                applicationRepo.save(application);
                id = application.getId();

                Optional<School> school = schoolRepo.findDistinctByPosition(position);
                SchoolPositionDto schoolPositionDto = convert(school.get());
                School tmp = schoolRepo.findById(schoolPositionDto.getSchoolId()).get();
                emailSender.send(applicant.getEmail(), applicantEmail.sendEmail(tmp.getName(),
                                tmp.getEmail(), tmp.getPhoneNumber()),
                        "Eingang Bestätigung");
                emailSender.send(tmp.getEmail(), schoolEmail.sendEmail(position.getJobTitel(),
                                applicant.getName(), applicant.getEmail(), applicant.getPhoneNumber()),
                        "Bewerbung Eingang");
            }
        }
        return id;
    }

    /* *
     * @param applicationFile : Liste von Bewerbungsfile
     *  @param applicantId : Bewerber ID
     *  @param applicationId : Bewerbungsid
     * diese Methode wird nur für bewerber mit einen Account verwendet, um die Bewerbungsunterlagen zu speichern
     */
    public boolean saveFile(List<MultipartFile> applicationFile, Long applicationId, Long applicantId)
            throws IOException {
        Application application = applicationRepo.findById(applicationId).get();
        boolean erg = false;
        for (int i = 0; i < applicationFile.size(); i++) {
            if (application != null) {
                ApplicationFile getFile = applicationFileRepo.save(ApplicationFile.builder()
                        .name(applicationFile.get(i).getOriginalFilename())
                        .type(applicationFile.get(i).getContentType())
                        .file(FileUtility.compressFile(applicationFile.get(i).getBytes())).build());
                ApplicationFile tmp = applicationFileRepo.findById(getFile.getId()).get();
                tmp.setApplicant(applicantRepo.findById(applicantId).get());
                tmp.setApplication(applicationRepo.findById(applicationId).get());
                erg = true;
            } else {
                erg = false;
            }
        }

        return erg;
    }

    /* *
     * @param applicationFile : Liste von Bewerbungsfile
     *  @param applicationId : Bewerbungsid
     * diese Methode wird nur für bewerber ohne Account verwendet, um die Bewerbungsunterlagen zu speichern
     */
    public ApplicationFile saveFileNoAccount(MultipartFile applicationFile, Long applicationId) throws IOException {
        NoAccountApplication noAccountApplication = noAccountApplicationRepo.findById(applicationId).get();
        ApplicationFile applicationFile1 = null;
        if (noAccountApplication != null) {
            applicationFile1 = applicationFileRepo.save(ApplicationFile.builder()
                    .name(applicationFile.getOriginalFilename())
                    .type(applicationFile.getContentType())
                    .file(FileUtility.compressFile(applicationFile.getBytes())).build());

        }
        return applicationFile1;
    }

    /* *
     * Alle abgeholte Infrormation aus dem Formular werden verwendet,
     * um die Bewerbungs für Bewerber ohne Account zu speichern.
     */
    public String createNoAccountApplication(String name, String firstName, String dayOfBirth,
                                             String email, String gender, String phoneNumber, String streetNumber,
                                             int postcode, String city, String country, Long positionId, List<MultipartFile> files) throws IOException {
        String tmpId = "";

        if (name.length() > 0 && firstName.length() > 0 && dayOfBirth.length() > 0 && email.length() > 0
                && email.length() > 0 && gender.length() > 0 && phoneNumber.length() > 0) {

            NoAccountApplication noAccountApplication = new NoAccountApplication(name, firstName, dayOfBirth,
                    email, gender, phoneNumber, streetNumber, postcode, city, country);

            noAccountApplication.setPosition(positionRepo.findById(positionId).get());

            noAccountApplicationRepo.save(noAccountApplication);
            for (int i = 0; i < files.size(); i++) {
                ApplicationFile applicationFile = saveFileNoAccount(files.get(i), noAccountApplication.getId());
                applicationFile.setNoAccountApplication(noAccountApplication);
                applicationFileRepo.save(applicationFile);
            }

            tmpId = "" + noAccountApplication.getId();
            Position position = positionRepo.findById(positionId).get();
            Optional<School> school = schoolRepo.findDistinctByPosition(position);
            SchoolPositionDto schoolPositionDto = convert(school.get());
            School tmp = schoolRepo.findById(schoolPositionDto.getSchoolId()).get();
            emailSender.send(email, applicantEmail.sendEmail(tmp.getName(),
                            tmp.getEmail(), tmp.getPhoneNumber()),
                    "Eingang Bestätigung");

            emailSender.send(tmp.getEmail(), schoolEmail.sendEmail(position.getJobTitel(), name, email, phoneNumber),
                    "Bewerbung Eingang");
        }
        return tmpId;
    }

    /* *
     * @param school: Object school
     * mit SchoolPositionDto werden ausgewählte Attribute des Objekts school als Object ausgegeben.
     */
    public SchoolPositionDto convert(School school) {
        SchoolPositionDto schoolPositionDto = new SchoolPositionDto();
        schoolPositionDto.setSchoolId(school.getId());
        return schoolPositionDto;

    }


}
