package com.bewerbungWebsite.aidp.controllers;

import com.bewerbungWebsite.aidp.Responses.MessageResponse;
import com.bewerbungWebsite.aidp.repository.PositionRepo;
import com.bewerbungWebsite.aidp.services.ApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping(path = "/api/application")
public class ApplicationController {

    @Autowired
    ApplicationService applicationService;

    @Autowired
    PositionRepo positionRepo;

    @PostMapping("/createApplication/{positionId}")
    public ResponseEntity<?> createApplication(@RequestParam("files") List<MultipartFile> files,
                                               @RequestParam("applicantId") String applicantId,
                                               @PathVariable Long positionId) throws IOException, NumberFormatException {

        try {
            Long userId = Long.parseLong(applicantId);
            LocalDateTime date = applicationService.generateDate();
            Long id = applicationService.createApplication(userId,
                    date, positionId);
            boolean saveFile = applicationService.saveFile(files, id, userId);

            if (saveFile) {
                return ResponseEntity.ok(new MessageResponse("du hast dich erfolgreich bewerben"));
            } else {
                return ResponseEntity.ok(new MessageResponse("du hast dich nicht erfolgreich bewerben"));
            }
        } catch (NumberFormatException n) {
            return ResponseEntity.ok(new MessageResponse("ApplicantId ist kein Zahl"));
        }


    }

    @PostMapping("/createNoAccountApplication/{positionId}")
    public ResponseEntity<?> createNoAccountApplication(@RequestParam("files") List<MultipartFile> files, @PathVariable Long positionId,
                                                        @RequestParam("name") String name,
                                                        @RequestParam("firstName") String firstname,
                                                        @RequestParam("dayOfBirth") String dayOfBirth,
                                                        @RequestParam("email") String email,
                                                        @RequestParam("phoneNumber") String phoneNumber,
                                                        @RequestParam("streetNumber") String streetNumber,
                                                        @RequestParam("postCode") String postCode,
                                                        @RequestParam("city") String city,
                                                        @RequestParam("country") String country,
                                                        @RequestParam("gender") String gender)
            throws IOException, NumberFormatException {
        int postalCode = 0;
        try {
            postalCode = Integer.parseInt(postCode);
            String number = applicationService.createNoAccountApplication(name, firstname, dayOfBirth, email, phoneNumber, gender,
                    streetNumber, postalCode, city, country, positionId, files);
            if (!number.isEmpty()) {
                return ResponseEntity.ok(new MessageResponse("du hast dich erfolgreich bewerben"));
            } else {
                return ResponseEntity.ok(new MessageResponse("du hast dich nicht erfolgreich bewerben"));
            }
        } catch (NumberFormatException n) {
            return ResponseEntity.ok(new MessageResponse("Postleihzahl ist kein Zahl"));
        }


    }


}
