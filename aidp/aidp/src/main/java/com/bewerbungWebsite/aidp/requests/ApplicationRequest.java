package com.bewerbungWebsite.aidp.requests;


import lombok.*;

@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class ApplicationRequest {

    // permet de recupere une infor precise sur le request boby. anfrage qui vient du frontend.
    private Long applicantId;
    private Long positionId;

    public Long getApplicantId() {
        return applicantId;
    }

    public void setApplicantId(Long applicantId) {
        this.applicantId = applicantId;
    }

    public Long getPositionId() {
        return positionId;
    }

    public void setPositionId(Long positionId) {
        this.positionId = positionId;
    }
}
