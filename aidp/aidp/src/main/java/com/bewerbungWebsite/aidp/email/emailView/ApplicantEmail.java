package com.bewerbungWebsite.aidp.email.emailView;

import org.springframework.stereotype.Component;

@Component
public class ApplicantEmail {
    public String sendEmail(String name, String email, String phoneNumber) {
        return "<!DOCTYPE html>\n" +
                "\n" +
                "<html lang=\"de\">\n" +
                "\n" +
                "<head>\n" +
                "\n" +
                "<meta charset=\"UTF-8\" />\n" +
                "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\n" +
                "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n" +
                "<title>MUO - Technology, Simplified</title>\n" +
                "    <style>\n" +
                "    \n" +
                "        body{\n" +
                "            font-family: sans-serif;\n" +
                "            background-color: ivory;\n" +
                "            font-size: 19px;\n" +
                "            margin: 0, auto;\n" +
                "            padding: 3%;\n" +
                "        }\n" +
                "            .container{\n" +
                "                height: 500px;\n" +
                "                width: 450px;\n" +
                "            }\n" +
                "    </style>\n" +
                "</head>\n" +
                "    <body>\n" +
                "        <div class=\"container\">\n" +
                "            <h2>Vielen Dank für Ihre Bewerbung!</h2>\n" +
                "            <p>Vielen Dank für die Zusendung Ihre Bewerbungsunterlagen und das damit verbundene Interesse. \n" +
                "                wir werden uns sobald als möglich bei Ihnen melden. Wir bitten sie um etwas Geduld, \n" +
                "                bis wir die Bewerberinnen und Bewerber ermitteln haben, welche wir für eine Vorstellungsgespräch einladen werden.\n" +
                "            </p>\n" +
                "            <p>Information !</p>\n" +
                "            <ul>\n" +
                "                <li>" + name + "</li>\n" +
                "                <li>" + email + "</li>\n" +
                "                <li>" + phoneNumber + "</li>\n" +
                "            </ul>\n" +
                "            <p>Mit freundliche Grüße</p>\n" +
                "            <p>" + name + "</p>  \n" +
                "        </div>\n" +
                "    </body>\n" +
                "</html>\n";
    }
}
