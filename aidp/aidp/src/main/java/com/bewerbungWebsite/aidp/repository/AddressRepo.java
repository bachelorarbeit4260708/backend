package com.bewerbungWebsite.aidp.repository;

import com.bewerbungWebsite.aidp.models.Address;
import org.springframework.data.jpa.repository.JpaRepository;


public interface AddressRepo extends JpaRepository<Address,Long> {

}
