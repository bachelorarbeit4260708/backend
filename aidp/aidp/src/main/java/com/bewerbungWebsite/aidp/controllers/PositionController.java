package com.bewerbungWebsite.aidp.controllers;

import com.bewerbungWebsite.aidp.Responses.MessageResponse;
import com.bewerbungWebsite.aidp.dto.SchoolPositionDto;
import com.bewerbungWebsite.aidp.exception.ResourceNotFoundException;
import com.bewerbungWebsite.aidp.models.Position;
import com.bewerbungWebsite.aidp.models.School;
import com.bewerbungWebsite.aidp.repository.PositionRepo;
import com.bewerbungWebsite.aidp.repository.SchoolRepo;
import com.bewerbungWebsite.aidp.requests.PositionRequest;
import com.bewerbungWebsite.aidp.services.PositionService;
import com.bewerbungWebsite.aidp.services.UserTopClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/position")
public class PositionController {

    @Autowired
    PositionRepo positionRepo;

    @Autowired
    PositionService positionService;


    @Autowired
    UserTopClassService userTopClassService;

    @Autowired
    SchoolRepo schoolRepo;


    @PostMapping("/createPosition/{id}")
    public ResponseEntity<?> createPosition(@RequestBody PositionRequest positionRequest, @PathVariable("id") Long id) {
        System.out.println(id);
        if (userTopClassService.schoolExistById(id) == true) {
            Position positionToSave = new Position();
            positionToSave.setJobTitel(positionRequest.getJobTitel());
            positionToSave.setJobDescription(positionRequest.getJobDescription());
            positionToSave.setOffer(positionRequest.getOffer());
            positionToSave.setYourProfile(positionRequest.getYourProfile());
            positionToSave.setContactPersonName(positionRequest.getContactPersonName());
            positionToSave.setContactPersonEmail(positionRequest.getContactPersonEmail());

            School school = schoolRepo.findById(id).get();

            school.getPosition().add(positionToSave);

            positionRepo.save(positionToSave);

            return ResponseEntity.ok(new MessageResponse("position wurde erfolgreich erzeugt"));

        } else {
            return ResponseEntity.ok(new MessageResponse("Konto existiert nicht"));
        }

    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getPositionById(@PathVariable Long id) throws ResourceNotFoundException {
        Position position = positionService.getPositionById(id);
        return new ResponseEntity<>(position, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/delete/{id}")
    public ResponseEntity<?> deletePosition(@PathVariable Long id) {
        boolean delete = positionService.deletePosition(id);
        if (delete) {
            return ResponseEntity.ok(new MessageResponse("Stelle wurde gelöscht"));
        }
        return ResponseEntity.ok(new MessageResponse("Stelle wurde nicht gelöscht"));
    }

    @PostMapping("/update")
    public ResponseEntity<?> updatePosition(@Valid @RequestBody Position position) {
        Position position1 = positionService.updatePosition(position);
        return new ResponseEntity<>(position1, new HttpHeaders(), HttpStatus.OK);
    }


    @GetMapping("/getAllPosition")
    public ResponseEntity<?> getAllPosition() {
        try {
            return new ResponseEntity<List<SchoolPositionDto>>(positionService.getAllPosition(), new HttpHeaders(), HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<Exception>(exception, new HttpHeaders(), HttpStatus.OK);
        }
    }

    @GetMapping("/getPosition")
    public ResponseEntity<?> getPosition() {
        List<SchoolPositionDto> twoPosition = positionService.getPosition();
        return new ResponseEntity<>(twoPosition, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/school/{id}")
    public ResponseEntity<?> getSchoolWithPosition(@PathVariable Long id) throws ResourceNotFoundException {
        School school = positionService.getSchoolWithPosition(id);
        return new ResponseEntity<>(school, new HttpHeaders(), HttpStatus.OK);
    }

}
