package com.bewerbungWebsite.aidp.models;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Application {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private LocalDateTime applicationDate;

    @OneToOne(cascade = CascadeType.ALL)
    private Position position;

    public Application(LocalDateTime applicationDate, Position position) {
        this.applicationDate = applicationDate;
        this.position = position;
    }

    public Application(LocalDateTime applicationDate) {
        this.applicationDate = applicationDate;

    }

    public Application() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(LocalDateTime applicationDate) {
        this.applicationDate = applicationDate;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
}
