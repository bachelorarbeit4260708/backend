package com.bewerbungWebsite.aidp.models;

import lombok.ToString;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@ToString
public class School extends UserTopClass {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String formationType;
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<Position> position;

    @OneToOne(cascade = CascadeType.ALL)
    private Address address;

    public School(String name, String email, String password, String phoneNumber, String formationType) {
        super(name, email, password, phoneNumber);
        this.formationType = formationType;
        this.position = new HashSet<Position>();

    }

    public School() {

    }

    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFormationType() {
        return formationType;
    }

    public void setFormationType(String formationType) {
        this.formationType = formationType;
    }

    public Set<Position> getPosition() {
        return position;
    }

    public void setPosition(Set<Position> position) {
        this.position = position;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
