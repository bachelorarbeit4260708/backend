package com.bewerbungWebsite.aidp.repository;

import com.bewerbungWebsite.aidp.models.Contact;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ContactRepo extends JpaRepository<Contact, Long> {

    Optional<Contact> findById(Long id);
}
