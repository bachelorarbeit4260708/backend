package com.bewerbungWebsite.aidp.requests;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class PositionRequest {

    private Long id;
    private String jobTitel;
    private String jobDescription;
    private String offer;
    private String yourProfile;
    private String contactPersonName;
    private String contactPersonEmail;

    public String getJobTitel() {
        return jobTitel;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setJobTitel(String jopTitel) {
        this.jobTitel = jopTitel;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    public String getOffer() {
        return offer;
    }

    public void setOffer(String offer) {
        this.offer = offer;
    }

    public String getYourProfile() {
        return yourProfile;
    }

    public void setYourProfile(String yourProfil) {
        this.yourProfile = yourProfile;
    }

    public String getContactPersonName() {
        return contactPersonName;
    }

    public void setContactPersonName(String contactPersonName) {
        this.contactPersonName = contactPersonName;
    }

    public String getContactPersonEmail() {
        return contactPersonEmail;
    }

    public void setContactPersonEmail(String contactPersonEmail) {
        this.contactPersonEmail = contactPersonEmail;
    }


}
