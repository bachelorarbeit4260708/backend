package com.bewerbungWebsite.aidp.repository;


import com.bewerbungWebsite.aidp.models.NoAccountApplication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NoAccountApplicationRepo extends JpaRepository<NoAccountApplication, Long> {

}
