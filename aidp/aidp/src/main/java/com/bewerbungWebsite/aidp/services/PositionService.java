package com.bewerbungWebsite.aidp.services;

import com.bewerbungWebsite.aidp.dto.SchoolPositionDto;
import com.bewerbungWebsite.aidp.exception.ResourceNotFoundException;
import com.bewerbungWebsite.aidp.models.Position;
import com.bewerbungWebsite.aidp.models.School;
import com.bewerbungWebsite.aidp.repository.PositionRepo;
import com.bewerbungWebsite.aidp.repository.SchoolRepo;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class PositionService {

    @Autowired
    PositionRepo positionRepo;

    @Autowired
    SchoolRepo schoolRepo;

    public Position getPositionById(Long id) throws ResourceNotFoundException {
        return positionRepo.findById(id).orElseThrow(() -> new ResourceNotFoundException("keine Stelle mit der Id :" + id + " gefunden"));
    }

    public Position updatePosition(Position newPosition) throws ResourceNotFoundException {

        Optional<Position> existPosition = positionRepo.findById(newPosition.getId());

        if (existPosition.isPresent()) {
            Position position = existPosition.get();
            position.setJobTitel(newPosition.getJobTitel());
            position.setJobDescription(newPosition.getJobDescription());
            position.setOffer(newPosition.getOffer());
            position.setYourProfile(newPosition.getYourProfile());
            position.setContactPersonName(newPosition.getContactPersonName());
            position.setContactPersonEmail(newPosition.getContactPersonEmail());

            return positionRepo.save(position);
        }

        return null;
    }

    public Position savePosition(Position position) {

        Optional<Position> pos = positionRepo.findById(position.getId());
        if (pos != null) {
            position.setJobTitel(position.getJobTitel());
            position.setJobDescription(position.getJobDescription());
            position.setOffer(position.getOffer());
            position.setYourProfile(position.getYourProfile());
            position.setContactPersonName(position.getContactPersonName());
            position.setContactPersonEmail(position.getContactPersonEmail());
        }

        return positionRepo.save(position);
    }


    public boolean deletePosition(Long id) {
        boolean erg = false;
        Position positionModel = positionRepo.findById(id).get();
        boolean exist = positionRepo.existsPositionById(id);
        if (exist) {
            positionRepo.delete(positionModel);
            erg = true;
        } else {
            erg = false;
        }
        return erg;
    }

    public List<SchoolPositionDto> getAllPosition() {
        List<SchoolPositionDto> positionList = new ArrayList<SchoolPositionDto>();
        positionRepo.findAll().forEach(position -> {
            Optional<School> school = schoolRepo.findDistinctByPosition(position);
            SchoolPositionDto schoolPositionDto = convert(school.get(), position);
            positionList.add(schoolPositionDto);
        });
        return positionList;
    }


    public List<SchoolPositionDto> getPosition() {
        List<SchoolPositionDto> twoPosition = getAllPosition();
        return twoPosition.stream().limit(2).collect(Collectors.toList());
    }


    public SchoolPositionDto convert(School school, Position position) {

        SchoolPositionDto schoolPositionDto = new SchoolPositionDto();
        schoolPositionDto.setSchoolId(school.getId());
        schoolPositionDto.setSchoolName(school.getName());
        schoolPositionDto.setSchoolAddress(school.getAddress());
        schoolPositionDto.setFormationType(school.getFormationType());
        schoolPositionDto.setPosition(position);
        return schoolPositionDto;

    }

    public School getSchoolWithPosition(Long SchoolId) throws ResourceNotFoundException {
        return schoolRepo.findById(SchoolId).orElseThrow(() -> new ResourceNotFoundException("keine Stelle mit der Id :" + SchoolId + " gefunden"));
    }

}
