package com.bewerbungWebsite.aidp.repository;
import com.bewerbungWebsite.aidp.models.Application;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ApplicationRepo extends JpaRepository<Application, Long> {

}
