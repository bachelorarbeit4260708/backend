package com.bewerbungWebsite.aidp.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
public class NoAccountApplication {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    @NotBlank
    private String name;

    @Column
    @NotBlank
    private String firstName;


    @Column
    @NotBlank
    private String dayOfBirth;

    @Column
    @NotBlank
    private String email;

    @Column
    @NotBlank
    private String gender;

    @Column
    @NotBlank
    private String phoneNumber;

    @Column
    private String streetNumber;

    @Column
    private int postcode;

    @Column
    private String city;

    @Column
    private String country;

    @OneToOne(cascade = CascadeType.ALL)
    private Position position;

    public NoAccountApplication(String name, String firstName, String dayOfBirth, String email,
                                String gender, String phoneNumber, String streetNumber, int postcode,
                                String city, String country) {
        this.name = name;
        this.firstName = firstName;
        this.dayOfBirth = dayOfBirth;
        this.email = email;
        this.gender = gender;
        this.phoneNumber = phoneNumber;
        this.streetNumber = streetNumber;
        this.postcode = postcode;
        this.city = city;
        this.country = country;
    }

    public NoAccountApplication() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getDayOfBirth() {
        return dayOfBirth;
    }

    public void setDayOfBirth(String dayOfBirth) {
        this.dayOfBirth = dayOfBirth;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public int getPostcode() {
        return postcode;
    }

    public void setPostcode(int postcode) {
        this.postcode = postcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
}
