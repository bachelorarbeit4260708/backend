package com.bewerbungWebsite.aidp.repository;

import com.bewerbungWebsite.aidp.models.ApplicationFile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ApplicationFileRepo extends JpaRepository<ApplicationFile, Long> {

}
