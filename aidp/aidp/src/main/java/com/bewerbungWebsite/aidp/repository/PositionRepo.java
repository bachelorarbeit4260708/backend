package com.bewerbungWebsite.aidp.repository;

import com.bewerbungWebsite.aidp.models.Position;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



import java.util.Optional;

@Repository
public interface PositionRepo extends JpaRepository<Position, Long> {

    Optional<Position> findById(Long id);

    Iterable<Object> findTopByOrderByIdAsc();

    Boolean existsPositionById(Long id);
}
