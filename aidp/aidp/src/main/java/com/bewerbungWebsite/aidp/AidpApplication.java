package com.bewerbungWebsite.aidp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AidpApplication {

	public static void main(String[] args) {
		SpringApplication.run(AidpApplication.class, args);
	}

}
