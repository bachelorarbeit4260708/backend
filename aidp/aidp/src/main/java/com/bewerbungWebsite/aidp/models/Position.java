package com.bewerbungWebsite.aidp.models;

import javax.persistence.*;


@Entity
public class Position {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @Lob
    @Column()
    private String jobTitel;
    @Lob
    @Column(length = 100000)
    private String jobDescription;
    @Lob
    @Column(length = 100000)
    private String offer;
    @Lob
    @Column(length = 100000)
    private String yourProfile;
    @Column
    private String contactPersonName;
    @Column
    private String contactPersonEmail;

    public Position(String jobTitel, String jobDescription, String offer, String yourProfile, String contactPersonName, String contactPersonEmail) {
        this.jobTitel = jobTitel;
        this.jobDescription = jobDescription;
        this.offer = offer;
        this.yourProfile = yourProfile;
        this.contactPersonName = contactPersonName;
        this.contactPersonEmail = contactPersonEmail;
    }

    public Position() {

    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getJobTitel() {
        return jobTitel;
    }

    public void setJobTitel(String jobTitel) {
        this.jobTitel = jobTitel;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    public String getOffer() {
        return offer;
    }

    public void setOffer(String offer) {
        this.offer = offer;
    }

    public String getYourProfile() {
        return yourProfile;
    }

    public void setYourProfile(String yourProfile) {
        this.yourProfile = yourProfile;
    }

    public String getContactPersonName() {
        return contactPersonName;
    }

    public void setContactPersonName(String contactPersonName) {
        this.contactPersonName = contactPersonName;
    }

    public String getContactPersonEmail() {
        return contactPersonEmail;
    }

    public void setContactPersonEmail(String contactPersonEmail) {
        this.contactPersonEmail = contactPersonEmail;
    }
}
