package com.bewerbungWebsite.aidp.controllers;

import com.bewerbungWebsite.aidp.Responses.MessageResponse;
import com.bewerbungWebsite.aidp.exception.Error;
import com.bewerbungWebsite.aidp.models.Address;
import com.bewerbungWebsite.aidp.models.Applicant;
import com.bewerbungWebsite.aidp.models.Contact;
import com.bewerbungWebsite.aidp.models.School;
import com.bewerbungWebsite.aidp.repository.AddressRepo;
import com.bewerbungWebsite.aidp.repository.ApplicantRepo;
import com.bewerbungWebsite.aidp.repository.ContactRepo;
import com.bewerbungWebsite.aidp.repository.SchoolRepo;
import com.bewerbungWebsite.aidp.requests.*;
import com.bewerbungWebsite.aidp.services.UserTopClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping(path = "/api/auth")
public class AuthController {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    SchoolRepo schoolRepo;

    @Autowired
    ApplicantRepo applicantRepo;

    @Autowired
    UserTopClassService userTopClassService;

    @Autowired
    ContactRepo contactRepo;

    @Autowired
    AddressRepo addressRepo;

    // signupSchule methode
    @PostMapping("/signUpSchool")
    public ResponseEntity<?> signUpSchool(@Valid @RequestBody SignUpSchoolRequest signUpSchoolRequest) {

        if (schoolRepo.existsByEmail(signUpSchoolRequest.getEmail())) {
            return ResponseEntity.status(new Error(402).getE())
                    .body(new MessageResponse("Ein Konto existiert bereits für diese E-Mail"));
        }

        // create object schule
        School school = new School(
                signUpSchoolRequest.getName(),
                signUpSchoolRequest.getEmail(),
                passwordEncoder.encode(signUpSchoolRequest.getPassword()),
                signUpSchoolRequest.getPhoneNumber(),
                signUpSchoolRequest.getFormationType()
        );
        Address address = new Address();
        addressRepo.save(address);

        // save Schule in database
        schoolRepo.save(school);
        school.setAddress(address);
        schoolRepo.save(school);
        return ResponseEntity.ok(new MessageResponse("Sie wurden erfolgreich registriert!"));
    }


    @PostMapping("/signUpApplicant")
    public ResponseEntity<?> signUpApplicant(@Valid @RequestBody SignUpApplicantRequest signUpApplicantRequest) {

        if (applicantRepo.existsByEmail(signUpApplicantRequest.getEmail())) {
            return ResponseEntity.status(new Error(402).getE())
                    .body(new MessageResponse("Ein Konto existiert bereits für diese E-Mail"));
        }

        // Erzeugung einem Objekt Bewerber
        Applicant applicant = new Applicant(
                signUpApplicantRequest.getName(),
                signUpApplicantRequest.getEmail(),
                passwordEncoder.encode(signUpApplicantRequest.getPassword())
        );
        Address address = new Address();
        addressRepo.save(address);

        //Speichern ein Bewerber in der Datenbank
        applicantRepo.save(applicant);
        applicant.setAddress(address);
        applicantRepo.save(applicant);
        return ResponseEntity.ok(new MessageResponse("Sie wurden erfolgreich registriert!"));
    }


    @PostMapping("/signInApplicant")
    public ResponseEntity<?> signInUser(@Valid @RequestBody SingInApplicantRequest singInApplicantRequest) {
        return new ResponseEntity<>(userTopClassService.getApplicantByEmailAndPassword(singInApplicantRequest.getEmail(), singInApplicantRequest.getPassword()),
                HttpStatus.OK);
    }

    @PostMapping("/signinSchool")
    public ResponseEntity<?> signInSchool(@Valid @RequestBody SingInApplicantRequest singInApplicantRequest) {

        return new ResponseEntity<>(userTopClassService.getSchoolByEmailAndPassword(singInApplicantRequest.getEmail(), singInApplicantRequest.getPassword()),
                HttpStatus.OK);
    }

    // Get  Ausbildungsplatzanbieter Profildaten mit Hilfe der  ID
    @GetMapping("/getSchoolProfilData/{id}")
    public ResponseEntity<School> getSchoolById(@PathVariable Long id) {
        School school = userTopClassService.getSchoolById(id);
        return new ResponseEntity<>(school, new HttpHeaders(), HttpStatus.OK);
    }

    // Get  Bewerber Profildaten mit Hilfe der  ID
    @GetMapping("/getApplicantProfilData/{id}")
    public ResponseEntity<Applicant> getApplicantById(@PathVariable Long id) {
        Applicant applicant = userTopClassService.getApplicantById(id);
        return new ResponseEntity<>(applicant, new HttpHeaders(), HttpStatus.OK);
    }

    // Update Ausbildungsplatzanbieter Profile mit Hilfe der Ausbildungsplatzanbieter ID
    @PutMapping("/putSchoolProfilData/{id}")
    public ResponseEntity<?> updateSchoolProfileData(@PathVariable Long id, @Valid @RequestBody SignUpSchoolRequest
            signUpSchoolRequest) {
        if (schoolRepo.existsById(id)) {
            Address address = new Address(signUpSchoolRequest.getStreetNumber(), signUpSchoolRequest.getPostcode(),
                    signUpSchoolRequest.getCity(), signUpSchoolRequest.getCountry());
            userTopClassService.updateSchoolProfile(id, signUpSchoolRequest.getName(), address, signUpSchoolRequest.getEmail(),
                    signUpSchoolRequest.getPhoneNumber(), signUpSchoolRequest.getFormationType());

            return ResponseEntity.status(HttpStatus.OK).body(
                    new MessageResponse("Daten wurde erfolgreich update"));

        } else {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(
                    new MessageResponse("Daten wurde nicht update"));
        }
    }

    // Update Bewerber Profil mit Hilfe der Bewerber ID
    @PutMapping("/putApplicantProfilData/{id}")
    public ResponseEntity<?> updateApplicantProfileData(@PathVariable Long id, @Valid @RequestBody ApplicantRequest applicantRequest) {
        if (applicantRepo.existsById(id)) {
            Address address = new Address(applicantRequest.getStreetNumber(), applicantRequest.getPostcode(), applicantRequest.getCity(), applicantRequest.getCountry());
            userTopClassService.updateApplicantProfile(id, applicantRequest.getName(), applicantRequest.getFirstName(),
                    applicantRequest.getEmail(), applicantRequest.getDayOfBirth(), applicantRequest.getGender(),
                    applicantRequest.getPhoneNumber(), address);
            return ResponseEntity.status(HttpStatus.OK).body(
                    new MessageResponse("Daten wurden erfolgreich update"));
        } else {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(
                    new MessageResponse("Daten wurde nicht update"));
        }
    }

    @PostMapping("/contact")
    public ResponseEntity<?> sendContactData(@RequestBody ContactRequest contactRequest) {
        Contact contact = new Contact(
                contactRequest.getName(),
                contactRequest.getEmail(),
                contactRequest.getMessage()
        );
        contactRepo.save(contact);
        return ResponseEntity.ok(new MessageResponse("Kontakt Formular wurde erfolgreich geschickt"));
    }
}



