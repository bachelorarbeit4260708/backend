package com.bewerbungWebsite.aidp.exception;

public class Error {
    int e;
    String errorText;

    public Error(int e) {
        this.e = e;
        this.setErrorText();
    }

    public int getE() {
        return e;
    }

    public Error getError() {
        return this;
    }

    private void setErrorText() {
        switch (this.e) {
            case 401:
                this.errorText = "error: Schon angemeldet";
            case 402:
                this.errorText = "error: Ein Konton existiert bereits für diese E-Mail";
        }
    }


}
