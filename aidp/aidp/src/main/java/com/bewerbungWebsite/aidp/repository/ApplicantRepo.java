// @Auto: Daniele Regene Fofack Woufack

package com.bewerbungWebsite.aidp.repository;
import com.bewerbungWebsite.aidp.models.Applicant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;


@Repository
@Transactional(readOnly = true)
public interface ApplicantRepo extends JpaRepository<Applicant, Long> {

    /* find user by
    *@param id
    * @param email
    * @param name
     */
    Optional<Applicant> findById(Long id);
    Optional<Applicant> findByEmail(String email);
    Optional<Applicant> findByName(String name);


    /* check if  user exist  with
     * @param email
     * @param name
     */
    Boolean existsByEmail(String email);
    Boolean existsByName(String name);

    //Query
    @Query("SELECT U FROM Applicant U WHERE U.id=?1")
    Applicant deleteBewerberById(Long id);

    @Query("SELECT U FROM Applicant U WHERE U.email=?1 and U.password=?2")
    Optional<Applicant> getApplicantByEmailAndPassword(String email, String password);
}
