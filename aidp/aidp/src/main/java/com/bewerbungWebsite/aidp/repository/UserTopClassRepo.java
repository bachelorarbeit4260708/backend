package com.bewerbungWebsite.aidp.repository;

import com.bewerbungWebsite.aidp.models.UserTopClass;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;


@Repository
@Transactional(readOnly = true)
public interface UserTopClassRepo extends JpaRepository<UserTopClass, Long> {
    Optional<UserTopClass> findById(Long id);
    Optional<UserTopClass> findByEmail(String email);
}
