package com.bewerbungWebsite.aidp.email;

import javax.annotation.PostConstruct;

public interface EmailSender {
    @PostConstruct
    void send(String to, String mail, String subject);
}
