package com.bewerbungWebsite.aidp.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApplicationFile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private String type;

    @Lob
    @Column(nullable = false)
    private byte[] file;

    @OneToOne(cascade = CascadeType.ALL)
    private Application application;

    @OneToOne(cascade = CascadeType.ALL)
    private Applicant applicant;

    @OneToOne(cascade = CascadeType.ALL)
    private NoAccountApplication noAccountApplication;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    public Applicant getApplicant() {
        return applicant;
    }

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }

    public NoAccountApplication getNoAccountApplication() {
        return noAccountApplication;
    }

    public void setNoAccountApplication(NoAccountApplication noAccountApplication) {
        this.noAccountApplication = noAccountApplication;
    }
}
