package com.bewerbungWebsite.aidp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {
    private String exceptionDetails;
    private Object filValues;

    public ResourceNotFoundException() {
        super();
    }

    public ResourceNotFoundException(String message) {
        super(message);
    }

    public ResourceNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public ResourceNotFoundException(String exceptionDetails, Long filValues) {
        super(exceptionDetails + " - " + filValues);
        this.exceptionDetails = exceptionDetails;
        this.filValues = filValues;
    }

    public String getExceptionDetails() {
        return exceptionDetails;
    }

    public Object getFilValues() {
        return filValues;
    }
}
