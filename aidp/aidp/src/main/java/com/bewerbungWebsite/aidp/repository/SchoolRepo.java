//@Autor: Daniele Regene Fofack Woufack

package com.bewerbungWebsite.aidp.repository;

import com.bewerbungWebsite.aidp.models.Position;
import com.bewerbungWebsite.aidp.models.School;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.Optional;
@Repository
@Transactional(readOnly = true)
public interface SchoolRepo extends JpaRepository<School, Long> {
    /* find user by
     *@param id
     * @param email
     * @param name
     * @param ausbildungArt
     */
    Optional<School> findById(Long id);
    Optional<School> findByEmail(String email);
    Optional<School> findByName(String name);
    Optional<School> findByFormationType(String formationType);

    /* check if  user exist  with
     * @param email
     * @param name
     * @param ausbildungArt
     */
    Boolean existsByEmail(String email);
    Boolean existsByName(String name);
    Boolean existsByFormationType(String formationType);
    @Query("SELECT U FROM School U WHERE U.id=?1")
    School deleteSchoolById(Long id);
    @Query("SELECT COUNT(U) FROM School U WHERE U.id=?1")
    long countById(Long id);

    // find school by position
    Optional<School> findDistinctByPosition(Position position);

}
