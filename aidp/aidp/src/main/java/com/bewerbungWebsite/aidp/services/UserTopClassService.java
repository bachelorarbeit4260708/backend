package com.bewerbungWebsite.aidp.services;


import com.bewerbungWebsite.aidp.exception.ResourceNotFoundException;
import com.bewerbungWebsite.aidp.models.Address;
import com.bewerbungWebsite.aidp.models.Applicant;
import com.bewerbungWebsite.aidp.models.School;
import com.bewerbungWebsite.aidp.models.UserTopClass;
import com.bewerbungWebsite.aidp.repository.UserTopClassRepo;
import com.bewerbungWebsite.aidp.repository.ApplicantRepo;
import com.bewerbungWebsite.aidp.repository.SchoolRepo;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@AllArgsConstructor
public  class UserTopClassService {

    @Autowired
    UserTopClassRepo userTopClassRepo;
    @Autowired
    ApplicantRepo applicantRepo;

    @Autowired
    SchoolRepo schoolRepo;

    // Bewerber Methoden
    public Applicant getApplicantById(Long id) throws ResourceNotFoundException {
        return applicantRepo.findById(id).orElseThrow(() -> new ResourceNotFoundException("bewerber not found"));
    }

    public Applicant getApplicantByEmail(String email) throws ResourceNotFoundException {
        return applicantRepo.findByEmail(email).orElseThrow(() -> new ResourceNotFoundException("bewerber not found"));
    }


    // Schule Methoden
    public School getSchoolById(Long id) throws ResourceNotFoundException {
        return schoolRepo.findById(id).orElseThrow(() -> new ResourceNotFoundException("Schule not found"));
    }

    public School getSchoolByEmail(String email) throws ResourceNotFoundException {
        return schoolRepo.findByEmail(email).orElseThrow(() -> new ResourceNotFoundException("Schule not found"));
    }

    public boolean schoolExistById(Long id) {
        return schoolRepo.countById(id) > 0;
    }

    // Benutzer Methoden
    public UserTopClass getUserTopClassById(Long id) throws ResourceNotFoundException {
        return userTopClassRepo.findById(id).orElseThrow(() -> new ResourceNotFoundException("Benutzer not found"));
    }

    public UserTopClass getUserTopClassByEmail(String email) throws ResourceNotFoundException {
        return userTopClassRepo.findByEmail(email).orElseThrow(() -> new ResourceNotFoundException("Benutzer not found"));
    }

    public UserDetails getLoggedUser() {
        return (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    public Applicant getApplicantByEmailAndPassword(String email, String password) throws ResourceNotFoundException {
        return applicantRepo.findByEmail(email).orElseThrow(() -> new ResourceNotFoundException("bewerber not found"));
    }

    public School getSchoolByEmailAndPassword(String email, String password) throws ResourceNotFoundException {
        return schoolRepo.findByEmail(email).orElseThrow(() -> new ResourceNotFoundException("bewerber not found"));
    }

    public void updateSchoolProfile(Long id, String name,Address address1, String email, String phoneNumber, String formationType) {
        School school = getSchoolById(id);
        if (name != null && name.length() > 0 && !Objects.equals(name, school.getName())) {
            school.setName(name);
        }

        if(address1 != null){
            school.getAddress().setStreetNumber(address1.getStreetNumber());
            school.getAddress().setPostcode(address1.getPostcode());
            school.getAddress().setCity(address1.getCity());
            school.getAddress().setCountry(address1.getCountry());
        }

        if (email != null && email.length() > 0 && !Objects.equals(email, school.getEmail())) {
            school.setEmail(email);
        }
        if (phoneNumber != null && phoneNumber.length() > 0 && !Objects.equals(phoneNumber, school.getPhoneNumber())) {
            school.setPhoneNumber(phoneNumber);
        }
        if (formationType != null && formationType.length() > 0 && !Objects.equals(email, school.getFormationType())) {
            school.setFormationType(formationType);
        }

        schoolRepo.save(school);
    }

    public void updateApplicantProfile(Long id, String name, String firstName, String email, String dayOfBirth,
                                       String gender, String phoneNumber, Address address){
        Applicant applicant = getApplicantById(id);

        if(name != null && name.length()>0 && !Objects.equals(name, applicant.getName())){
            applicant.setName(name);
        }

        if(firstName != null && firstName.length()>0 && !Objects.equals(firstName, applicant.getFirstName())){
            applicant.setFirstName(firstName);
        }

        if(email!= null && email.length()>0 && !Objects.equals(email, applicant.getEmail())){
            applicant.setEmail(email);
        }

        if(dayOfBirth != null && dayOfBirth.length()>0 && !Objects.equals(dayOfBirth, applicant.getDayOfBirth())){
          applicant.setDayOfBirth(dayOfBirth);
        }

        if(gender != null && gender.length() >0 && !Objects.equals(gender, applicant.getGender())){
            applicant.setGender(gender);
        }

        if(phoneNumber != null && phoneNumber.length() > 0  && !Objects.equals(phoneNumber, applicant.getPhoneNumber())){
            applicant.setPhoneNumber(phoneNumber);
        }

        if(address != null){

            applicant.getAddress().setStreetNumber(address.getStreetNumber());
            applicant.getAddress().setPostcode(address.getPostcode());
            applicant.getAddress().setCity(address.getCity());
            applicant.getAddress().setCountry(address.getCountry());
        }
        applicantRepo.save(applicant);
    }


}
